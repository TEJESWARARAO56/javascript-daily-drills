let timestamps = [0, 0, 0, 0];

function fetchUrl(url, headers, params) {
    let parameters = "?"
    for (const key in params) {
        parameters += key + "=" + params[key] + "&"
    }
    parameters = parameters.slice(0, -1);
    return new Promise((resolve, reject) => {
        fetch(`${url}${parameters}`, {
            method: 'GET',
            headers: headers
        }).then((response) => {
            resolve(response.json());
        }).catch((err) => {
            reject(err)
        })
    })
}

let url = "https://jsonplaceholder.typicode.com/users"
let numberOfRequests = 15;

for (let index = 0; index < numberOfRequests; index++) {
    let timestamp = new Date().getTime()
    timestamps[index % 3] = timestamp
    while (new Date().getTime() - timestamps[0] < 1000) {
    }
    console.log(index);
    fetchUrl(url, {}, { id: 1 })
        .then((data) => {
            // console.log(data)
        })
        .catch((err) => {
            console.error(err)
        })
}

// Another small drill on promises:

// You are working on a web application that fetches data from an external API. However, 
// due to the API's rate limit restrictions, you are only allowed to make a maximum of 3 requests per second. 
// Your task is to write a function that makes API requests and returns a Promise that resolves with the response data.

// The function should accept the following arguments:

// url (string): The URL to the API endpoint.
// headers (object, optional): An object containing any headers to be sent with the request.
// params (object, optional): An object containing any query parameters to be sent with the request.


// The function should meet the following requirements:

// - It should return a Promise that resolves with the response data.
// - It should respect the API's rate limit restrictions by ensuring that no more than 3 requests are made per 3 second.
// - It should handle any errors that may occur during the request.
// - You may use any external libraries or modules as needed.