const movies = [
  {
    title: "The Godfather",
    director: "Francis Ford Coppola",
    year: 1972,
    cast: ["Marlon Brando", "Al Pacino", "James Caan"],
    genre: ["Crime", "Drama"],
    rating: 9.2,
    duration: 175,
  },
  {
    title: "The Shawshank Redemption",
    director: "Frank Darabont",
    year: 1994,
    cast: ["Tim Robbins", "Morgan Freeman", "Bob Gunton"],
    genre: ["Drama"],
    rating: 9.3,
    duration: 142,
  },
  {
    title: "The Dark Knight",
    director: "Christopher Nolan",
    year: 2008,
    cast: ["Christian Bale", "Heath Ledger", "Aaron Eckhart"],
    genre: ["Action", "Crime", "Drama"],
    rating: 9.0,
    duration: 152,
  },
  {
    title: "Inception",
    director: "Christopher Nolan",
    year: 2010,
    cast: ["Leonardo DiCaprio", "Joseph Gordon-Levitt", "Ellen Page"],
    genre: ["Action", "Adventure", "Sci-Fi"],
    rating: 8.8,
    duration: 148,
  },
  {
    title: "The Matrix",
    director: "Lana Wachowski, Lilly Wachowski",
    year: 1999,
    cast: ["Keanu Reeves", "Laurence Fishburne", "Carrie-Anne Moss"],
    genre: ["Action", "Sci-Fi"],
    rating: 8.7,
    duration: 136,
  },
  {
    title: "Fight Club",
    director: "David Fincher",
    year: 1999,
    cast: ["Brad Pitt", "Edward Norton", "Helena Bonham Carter"],
    genre: ["Drama"],
    rating: 8.8,
    duration: 139,
  },
  {
    title: "Forrest Gump",
    director: "Robert Zemeckis",
    year: 1994,
    cast: ["Tom Hanks", "Robin Wright", "Gary Sinise"],
    genre: ["Drama", "Romance"],
    rating: 8.8,
    duration: 142,
  },
  {
    title: "Pulp Fiction",
    director: "Quentin Tarantino",
    year: 1994,
    cast: ["John Travolta", "Samuel L. Jackson", "Uma Thurman"],
    genre: ["Crime", "Drama"],
    rating: 8.9,
    duration: 154,
  },
  {
    title: "The Silence of the Lambs",
    director: "Jonathan Demme",
    year: 1991,
    cast: ["Jodie Foster", "Anthony Hopkins", "Lawrence A. Bonney"],
    genre: ["Crime", "Drama", "Thriller"],
    rating: 8.6,
    duration: 118,
  },
  {
    title: "The Usual Suspects",
    director: "Bryan Singer",
    year: 1995,
    cast: ["Kevin Spacey", "Gabriel Byrne", "Chazz Palminteri"],
    genre: ["Crime", "Mystery", "Thriller"],
    rating: 8.5,
    duration: 106,
  },
];

// 1. Write a function that takes in a genre and returns an array of movie titles that belong to that genre, sorted in descending order of rating.
function getMoviesByGenre(genre) {
  return movies.filter((movie) => movie.genre.includes(genre))
    .sort((movie1, movie2) => movie2.rating - movie1.rating)
    .map((movie) => movie.title)
}
console.log(getMoviesByGenre("Action"));
// expected output: 
// [
//   "The Dark Knight",
//   "Inception",
//   "The Matrix",
//   "The Lord of the Rings: The Return of the King",
//   "The Avengers",
//   "Terminator 2: Judgment Day"
// ]

// 2. Write a function that takes in an actor's name and returns an array of movie titles that the actor appeared in, sorted by year of release.
function getMoviesByActor(actor) {
  return movies.filter((movie) => movie.cast.includes(actor))
    .sort((movie1, movie2) => movie1.year - movie2.year)
    .map((movie) => movie.title)
}
console.log(getMoviesByActor("Leonardo DiCaprio"));

// expected output: 
// [
//   "Titanic",
//   "The Departed",
//   "The Wolf of Wall Street",
//   "The Revenant"
// ]


// 3. Write a function that takes in a director's name and returns an object with the director's name as -
// - a key and an array of movie titles that the director directed as the value.
function getMoviesByDirector(director) {
  let moviesByDirector = {}
  moviesByDirector[director] = movies.filter((movie) => movie.director === director)
    .map((movie) => movie.title)
  return moviesByDirector
}
console.log(getMoviesByDirector("Christopher Nolan"));

// expected output: 
// {
//   "Christopher Nolan": [
//     "The Dark Knight",
//     "Inception",
//     "Interstellar",
//     "Dunkirk",
//     "Memento"
//   ]
// }


// 4. Write a function that takes in a year and returns an object with the year as a key and an array of movie 
// titles released in that year as the value, sorted in descending order of rating.
function getMoviesByYear(year) {
  let moviesByYear = {}
  moviesByYear[year] = movies.filter((movie) => movie.year === year)
    .sort((movie1, movie2) => movie2.rating - movie1.rating)
    .map((movie) => movie.title)
  return moviesByYear;
}
console.log(getMoviesByYear(1994));

// expected output: 
// {
//   1994: [
//     "The Shawshank Redemption",
//     "Pulp Fiction",
//     "Forrest Gump"
//   ]
// }


// 5. Write a function that returns an array of objects, where each object contains a unique combination of director and genre. 
// Each object should have a "director" key and a "genre" key, and the values of these keys should be arrays of movie titles that
//  match the combination of director and genre. The movie titles should be sorted in descending order of rating.
function getMoviesByDirectorAndGenre() {
  // sorting movies by rating
  let moviesOrderedByTitles = movies.sort((movie1, movie2) => movie2.rating - movie1.rating)
  let moviesByGenreAndDirector = moviesOrderedByTitles.reduce((accumulator, movie) => {
    movie.genre.forEach(genre => {
      // checking object is present or not with director and genre
      let index = accumulator.findIndex((object) => {
        return object.genre === genre && object.director === movie.director
      })
      if (index > -1) {
        accumulator[index].movies.push(movie.title)
      } else {
        accumulator.push({
          director: movie.director,
          genre: genre,
          movies: [movie.title]
        })
      }
    });
    return accumulator
  }, [])
  return moviesByGenreAndDirector
}

console.log(getMoviesByDirectorAndGenre());


// expected output: 
// [
//   {
//     director: "Christopher Nolan",
//     genre: "Action",
//     movies: ["The Dark Knight"]
//   },
//   {
//     director: "Christopher Nolan",
//     genre: "Thriller",
//     movies: ["Inception"]
//   },
//   {
//     director: "Christopher Nolan",
//     genre: "Sci-Fi",
//     movies: ["Interstellar"]
//   },
//   {
//     director: "Steven Spielberg",
//     genre: "Action",
//     movies: ["Jurassic Park"]
//   },
//   {
//     director: "Steven Spielberg",
//     genre: "Drama",
//     movies: ["Schindler's List"]
//   },
//   {
//     director: "Steven Spielberg",
//     genre: "Adventure",
//     movies: ["Indiana Jones and the Raiders of the Lost Ark"]
//   }
// ]
