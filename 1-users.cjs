const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interests: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/
function getAllUsersWhoPlayVideoGames() {
    return Object.fromEntries(Object.entries(users).filter((user) => {
        return user[1].interests.some((interest) => interest.includes("Video Games"))

    }))
}
console.log(getAllUsersWhoPlayVideoGames())

function getAllUsersStayingInGermany() {
    return Object.fromEntries(Object.entries(users).filter((user) => {
        return user[1].nationality === "Germany"

    }))
}
console.log("Users Staying in Germany")
console.log(getAllUsersStayingInGermany())

function sortUsersOnSeniorLevel() {
    let designations = ["Intern", "dfgthtertgthy", "Senior",]
    return Object.fromEntries(Object.entries(users).sort((user1, user2) => {
        let user1Rank = designations.findIndex((designation) => {
            return user1[1]["desgination"].includes(designation)
        })
        user1Rank = user1Rank === -1 ? 1 : user1Rank
        let user2Rank = designations.findIndex((designation) => {
            return user2[1]["desgination"].includes(designation)
        })

        user2Rank = user2Rank === -1 ? 1 : user2Rank
        console.log(user1Rank, user2Rank)
        if (user1Rank === user2Rank) {
            return user2[1]["age"] - user1[1]["age"]
        }
        return user2Rank - user1Rank
    }))
}
console.log("After sorting")
console.log(sortUsersOnSeniorLevel())

function findALlUsersWithMasterDegree() {
    return Object.fromEntries(Object.entries(users).filter((user) => {
        return user[1].qualification === "Masters"

    }))
}
console.log("kusers with master degree")
console.log(findALlUsersWithMasterDegree())

function groupUserBasedOnProgrmmingLanguage() {
    let programmingLanguages = ["Golang", "Javascript", "Python"]
    let userIneachProgramming = Object.entries(users).reduce((accumulator, user) => {
        let languageIndex = programmingLanguages.findIndex((language) => {
            return user[1]["desgination"].includes(language)
        })
        accumulator[programmingLanguages[languageIndex]] = accumulator[programmingLanguages[languageIndex]] || []
        accumulator[programmingLanguages[languageIndex]].push(user)
        return accumulator
    }, {})
    userIneachProgramming = Object.entries(userIneachProgramming)
    return Object.fromEntries(userIneachProgramming.map((language) => {
        return [language[0], Object.fromEntries(language[1])]
    }))
}

console.log(groupUserBasedOnProgrmmingLanguage())