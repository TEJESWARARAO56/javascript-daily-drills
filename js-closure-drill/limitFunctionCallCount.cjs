function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    if (arguments.length < 2 || typeof cb != 'function' || typeof n != "number") {
        throw new Error("parameters not defined")
    }
    let count = 0;
    function callsCb(...args) {
        if (count < n) {
            count++
            return cb(...args)
        } else {
            return null
        }
    }
    return callsCb
}

module.exports = limitFunctionCallCount;
// function cb(...args) {
//     return args
// }
// let x = limitFunctionCallCount(cb);
// console.log(x(7, 8, 9));
// console.log(x(6, 7, 8));
// console.log(x(4, 5, 6));
// console.log(x(1));
