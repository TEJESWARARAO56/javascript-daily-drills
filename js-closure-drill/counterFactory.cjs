function counterFactory() {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.
    let count = 0
    return {
        increment: function () {
            return ++count
        }, decrement: function () {
            return --count
        }
    }
}

// let x = counterFactory()

// console.log(x.increment())

// console.log(x.increment())

// console.log(x.decrement())

module.exports = counterFactory


/*
    Create a function for each problem in a file called
        counterFactory.cjs
        limitFunctionCallCount.cjs
        cacheFunction.cjs
    and so on in the root of the project.
    
    Ensure that the functions in each file is exported and tested in its own file called
        testCounterFactory.cjs
        testLimitFunctionCallCount.cjs
        testCacheFunction.cjs
    and so on in a folder called test.

    Create a new git repo on gitlab for this project, ensure that you commit after you complete each problem in the project. 
    Ensure that the repo is a public repo.

    When you are done, send the gitlab url to your mentor
*/